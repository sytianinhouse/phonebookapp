import { Component, OnInit } from '@angular/core';

import { Platform, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppService } from './services/app.service';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Network } from '@ionic-native/network/ngx';
import { ToastService } from './services/toast.service';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { ActivatedRoute } from '@angular/router';
import { AbstractComponent } from './abstract.component';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent extends AbstractComponent implements OnInit {
  
  constructor(
    private platform: Platform,
    private network: Network,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private diagnostic: Diagnostic,
    private speechRecognition: SpeechRecognition,
    private screenOrientation: ScreenOrientation,
    public route: ActivatedRoute,
    public appService: AppService,
    private toastService: ToastService
  ) {
    super();
    this.initializeApp();
  }

  ngOnInit() {
    this.getOrientation();
    this.screenOrientation.onChange().subscribe(
      () => {
        this.getOrientation();
      }
    );
  }

  ngAfterViewInit() {
  }

  ionViewWillLeave() {
  }

  getOrientation() {
    let type = this.screenOrientation.type;
    let portraitFound = type.indexOf("portrait");
    let landscapeFound = type.indexOf("landscape");
    if (portraitFound > -1) {
      this.appService.currScreenOrientation = 'portrait';
    } else if (landscapeFound > -1) {
      this.appService.currScreenOrientation = 'landscape';
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      // watch network for a disconnection
      let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
        this.toastService.presentToast('Network was disconnected :-(');
      });
      
      this.speechRecognition.hasPermission()
        .then((hasPermission: boolean) => {
          if (!hasPermission) {
            this.speechRecognition.requestPermission()
              .then(
                () => console.log('Speech Granted'),
                () => console.log('Speech Denied')
              )
          }
        });
      
      this.diagnostic.isContactsAuthorized()
        .then((authorized) => {
            if (!authorized) {
              this.diagnostic.requestContactsAuthorization()
              .then((status) => {
                  console.log('Contact Authorized');
              }, function(error){
                  console.error('Contact authorization error' + error);
              });
            }
        }, function(error){
            console.error('Contact authorization error' + error);
        });

        
    });
  }
}
