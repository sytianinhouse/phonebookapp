import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

import { AppService } from '../../services/app.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {

  public _fetchDone: boolean = false;

  @Output() doFilter = new EventEmitter<any>();

  public mainFilter: any;

  constructor(
    private appService: AppService
  ) {
  }

  ngOnInit() {}

  fetchingDone() {
    this._fetchDone = true;
  }

  selectMainFilter(type, postType) {
    if (this._fetchDone) {
      this._fetchDone = false;
      this.mainFilter = type;
      let types = {
        'tax': type,
        'posttype': postType 
      }
      this.doFilter.emit(types);
    }
  }

  makeDefault(value, force = false) {
    if (!this.mainFilter || this.mainFilter == undefined || force) {
      this.mainFilter = value;
    }
  }
}
