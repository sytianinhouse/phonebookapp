import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef, Input } from '@angular/core';
import { AppService } from '../../services/app.service';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';
import { AbstractComponent } from '../../abstract.component';
import { AlertController, ToastController } from '@ionic/angular';
import { HeaderService } from '../../services/header.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent extends AbstractComponent {

  @Input('noSearch') noSearch: boolean = false;
  @Output() doSearch = new EventEmitter<any>();
  @Output() didFocus = new EventEmitter<any>();
  @Output() didBlur = new EventEmitter<any>();
  @Output() audioClicked = new EventEmitter<any>();
  @ViewChild('searchInput', { static: false }) myInput;
  @ViewChild('searchBtn', { static: false }) searchBtn;
  @ViewChild('audioBtn', { static: false }) audioBtn;
  
  public searchTimeout: any;
  public searchKey: any;
  public searchKeySlugged: any;

  public toastSpeech: any;
  public intervalAudio: any;

  constructor(
    public appService: AppService,
    private speechRecognition: SpeechRecognition,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public headerService: HeaderService
  ) {
    super();
    this.searchKey = this.headerService.searchKey;
    this.searchKeySlugged = this.headerService.sluggedSearchkey;
  }

  ionViewWillLeave() {
    this.toastSpeech.dismiss();
  }

  setFocus() {
    this.myInput.setFocus();
  }

  setSearchKey(value) {
    this.headerService.setBoth(value);
  }

  emitSearchKey() {
    let value = this.headerService.searchKey;
    this.headerService.setSearchKey(value);

    if (value) {
      this.headerService.setSluggedSearchKey(value);
    }

    this.doSearch.emit(value);
  }

  goDidFocus() {
    this.didFocus.emit();
  }

  goBlur() {
    this.didBlur.emit(this.headerService.sluggedSearchkey)
  }

  clickedSearch() {
    this.emitSearchKey();
  }

  searchTyping($event) {
    if (this.searchTimeout) {
      clearTimeout(this.searchTimeout);
    }

    this.searchTimeout = setTimeout(() => {
      this.emitSearchKey();
    }, 500);
  }

  clickAudio() {
    this.audioBtn.el.click();
  }

  startAudio() {
    if (this.headerService.currHeader == 'search-res') {
      this.speechRecognition.hasPermission()
        .then((hasPermission: boolean) => {
          if (hasPermission) {
            return this.startListen();
          } else {
            this.speechRecognition.requestPermission()
              .then(
                () => { this.startListen(); },
                () => { this.speechDeniedAlert() }
              )
          }
        })
    } else {
      this.audioClicked.emit(true);
    }
  }

  async speechDeniedAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
      subHeader: '',
      message: 'You need to allow this app to do speech recognition.',
      buttons: ['Close']
    });

    await alert.present();
  }

  startListen() {
    this.speechRecognition.startListening()
      .subscribe(
        (matches: string[]) => {
          if (matches.length > 0) {
            this.myInput.setFocus();
            this.headerService.setBoth(matches[0]);
            this.searchBtn.el.click();
          } else {
            this.speechErrorToast('We can`t hear you, please try again.')
          }
        },
        (onerror) => { this.speechErrorToast('Something went wrong in the process, please try again later.') }
      )
  }

  async speechErrorToast(message) {
    this.toastSpeech = await this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    this.toastSpeech.present();
  }
}
