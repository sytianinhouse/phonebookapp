import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from '../services/app.service';

import { AbstractComponent } from '../abstract.component';
import { FooterComponent } from '../partials/footer/footer.component';

import { Router } from '@angular/router';
import { CategoriesService } from '../services/categories.service';
import { ToastController, Platform, AlertController } from '@ionic/angular';
import { ToastService } from '../services/toast.service';
import { Network } from '@ionic-native/network/ngx';
import { HeaderService } from '../services/header.service';
import { Location } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage extends AbstractComponent implements OnInit {

  public mainFilter: any;
  public postTypeFilter: any;
  public fetching: boolean = true;
  public loadingMore: boolean = false;
  public errorToast: any;
  private refreshEvent: any;
  public ads: any;

  private backSubscription: any;

  public currentPage: number;
  public maxPage: number;
  public totalItems: number;

  @ViewChild(FooterComponent, { static: false })
  private footerComponent: FooterComponent;

  constructor(
    public appService: AppService,
    public categoriesService: CategoriesService,
    public toastService: ToastService,
    public headerService: HeaderService,
    private alertCtrl: AlertController,
    private location: Location,
    public inAppBrowser: InAppBrowser,
    public platform: Platform,
    public sanitizer: DomSanitizer,
    private router: Router,
  ) {
    super();
    this.platform.ready().then(() => {
      this.mainFilter = 'listings_dir_cat';
      this.postTypeFilter = 'listings_dir_ltg';
      // this.getCategories(this.mainFilter);
      this.getAds();
    });
  }

  ngOnInit() {
    this.platform.ready().then(() => {
      this.getAds();
    });
  }

  ionViewDidEnter() {
    this.backSubscription = this.platform.backButton.subscribe(() => {
      this.confirmQuit();
    });
  }

  ionViewWillEnter() {
    this.headerService.currHeader = 'home';
    this.footerComponent.fetchingDone();
  }

  ionViewWillLeave() {
    this.toastService.closeAny();
    this.closeRefresher();
    this.backSubscription.unsubscribe();
  }

  updateCategories(data = {}) {
    this.fetching = false;
    this.loadingMore = false;
    this.closeRefresher();
    this.footerComponent.fetchingDone();
    this.footerComponent.makeDefault(this.mainFilter);
    this.categoriesService.setCategories(data, this.currentPage);
  }

  loadMore() {
    this.loadingMore = true;
    this.getCategories(this.mainFilter, this.currentPage + 1);
  }

  getAds() {
    if (navigator.onLine) {
      this.categoriesService.apiAds()
        .then((data) => {
          let response = data[0];
          this.fetching = false;
          this.closeRefresher();
          this.ads = this.arrShuffle(response.data);
        })
        .catch((error) => {
          this.errorToast = this.toastService.presentToast('Something went wrong, please restart the app and try again.');
        });
    } else {
      this.errorToast = this.toastService.presentToast('You are offline, please connect to internet to continue.');
      this.fetching = false;
      this.closeRefresher();
    }
  }

  getCategories(type = null, page = 1) {
    if (navigator.onLine) {
      this.updateCategories([]);
      return;

      this.categoriesService.apiCategories(type, page)
        .then((data) => {
          let response = data[0];
          this.currentPage = parseInt(response.current_page);
          this.maxPage = parseInt(response.max_num_pages);
          let items = this.maxPage * 24;
          this.totalItems = items;

          this.updateCategories(response ? this.convertObjectToArr(response.data) : []);
        })
        .catch((error) => {
          this.updateCategories([]);
          this.errorToast = this.toastService.presentToast('Something went wrong, please restart the app and try again.');
        });
    } else {
      this.errorToast = this.toastService.presentToast('You are offline, please connect to internet to continue.');
      this.fetching = false;
      this.closeRefresher();
      this.updateCategories([]);
      setTimeout(() => {
        this.footerComponent.fetchingDone();
      }, 7000);
    }
  }

  itemClicked(id, name, slug, taxonomy, post_type) {
    this.router.navigate(['search-result', {
      categoryId: id, 
      categoryName: name, 
      categorySlug: slug, 
      taxonomy: taxonomy, 
      postType: post_type, 
      mainFilter: this.mainFilter, 
      fromCategory: true
    }]);
  }

  doRefresh(event) {
    this.refreshEvent = event;
    if (navigator.onLine) {
      this.getAds();
      // this.getCategories(this.mainFilter, 1);
    } else {
      this.errorToast = this.toastService.presentToast('You are offline, please connect to internet to continue.');
      this.ads = [];
      this.closeRefresher();
    }
  }

  closeRefresher() {
    if (this.refreshEvent) {
      this.refreshEvent.target.complete();
    }
  }

  goSearch(value: any) {
    this.router.navigate(['search-result', { taxonomy: this.mainFilter, postType: this.postTypeFilter, fromSearch: true, homeNavigate: true }]);
  }

  audioClicked(value: any) {
    this.router.navigate(['search-result', { taxonomy: this.mainFilter, postType: this.postTypeFilter, fromAudio: true, homeNavigate: true }]);
  }

  goFilter(value: any) {
    this.mainFilter = value.tax;
    this.postTypeFilter = value.posttype;
    // this.fetching = true;
    // this.getCategories(this.mainFilter);

    this.router.navigate(['search-result', { taxonomy: this.mainFilter, postType: this.postTypeFilter, homeSelection: true }]);
  }

  addClicked(ad) {
    if (ad.target_link.length > 0) {
      window.open(ad.target_link[0], '_system', 'location=yes');
    }
    return; 
  }

  async confirmQuit() {
    const confirm = await this.alertCtrl.create({
      header: 'Are you sure you want to quit?',
      message: '',
      buttons: [
        {
          text: 'No',
          handler: () => {
          }
        },
        {
          text: 'Yes',
          handler: () => {
            navigator['app'].exitApp(); // work in ionic 4
          }
        }
      ],
      animated: true
    });
    return await confirm.present();
  }
}
