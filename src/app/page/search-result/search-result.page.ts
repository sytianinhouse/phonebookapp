import { Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertController, ToastController, Platform } from '@ionic/angular';
import { AbstractComponent } from '../../abstract.component';
import { CategoriesService } from '../../services/categories.service';
import { FooterComponent } from '../../partials/footer/footer.component';
import { ToastService } from '../../services/toast.service';
import { PostService } from '../../services/post.service';
import { HeaderComponent } from '../../partials/header/header.component';
import { HeaderService } from '../../services/header.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.page.html',
  styleUrls: ['./search-result.page.scss'],
})
export class SearchResultPage extends AbstractComponent implements OnInit {
  
  public mainFilter: any;
  public postType: any;
  public fromSearch: any;
  public fromAudio: any;
  public fromHomeFooter: any;
  public fromHomeNavigate: any;

  public searchKey = '';
  public displaySearchKey = '';
  public sortingKey = 'alphabetical';

  public categoryName: any;
  public categorySlug = '';
  public categoryId: any;
  public taxonomy: any;
  public fromCategory: boolean = false;

  private refreshEvent: any;
  public errorToast: any;
  public fetching: boolean = true;
  public loadingMore: boolean = false;
  public notEmptyAlert: any;

  public currentPage: number;
  public maxPage: number;
  public totalItems: number;
  public belowScrollEvent: any;

  public catCurrentPage: number;
  public catMaxPage: number;
  public catTotalItems: number;

  public noSearchKey = true;

  @ViewChild(FooterComponent, { static: false })
  private footerComponent: FooterComponent;

  @ViewChild(HeaderComponent, { static: false })
  private headerComponent: HeaderComponent;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public alertCtrl: AlertController,
    private geolocation: Geolocation,
    public categoriesService: CategoriesService,
    public postService: PostService,
    public toastService: ToastService,
    public headerService: HeaderService
  ) { 
    super();
    this.displaySearchKey = '';
    this.categoryId = this.route.snapshot.params.categoryId;
    this.categoryName = this.route.snapshot.params.categoryName;
    this.categorySlug = this.route.snapshot.params.categorySlug;
    this.taxonomy = this.route.snapshot.params.taxonomy;
    this.postType = this.route.snapshot.params.postType;
    this.fromSearch = this.route.snapshot.params.fromSearch;
    this.fromAudio = this.route.snapshot.params.fromAudio;
    this.fromHomeFooter = this.route.snapshot.params.homeSelection;
    this.fromHomeNavigate = this.route.snapshot.params.homeNavigate;
    this.fromCategory = this.route.snapshot.params.fromCategory;

    if (this.categoryName) {
      this.searchKey = this.headerService.setSluggedSearchKey(this.categoryName);
      this.displaySearchKey = this.headerService.setSearchKey(this.categoryName);
    }

    if (this.fromSearch || this.fromAudio) { // For quick modify
      // this.categoriesService.setPost([]);
    }
  }

  ionViewDidEnter() {
    // Back navigate came from single page
    if (this.headerService.navigatedSearch) {
      this.fromSearch = this.headerService.navigatedSearch;
      this.headerService.setSearch(false);
    }

    if (this.headerService.navigatedAudio) {
      this.fromAudio = this.headerService.navigatedAudio;
      this.headerService.setAudio(false);
    }

    if (this.headerService.searchKey && this.headerService.sluggedSearchkey) {
      this.searchKey = this.headerService.sluggedSearchkey;
      this.displaySearchKey = this.headerService.searchKey;
      this.noSearchKey = false;
    }

    if (this.fromSearch) {
      if (this.fromHomeNavigate && this.searchKey == '') {
        this.noSearchKey = true;
        this.updatePosts([]);
        this.updateCats([]);
      }
      this.fromSearch = false;
      this.headerComponent.setFocus();
      this.footerComponent.fetchingDone();
      this.fetching = false;
    } else if (this.fromAudio) {
      if (this.fromHomeNavigate && this.searchKey == '') {
        this.noSearchKey = true;
        this.updatePosts([]);
        this.updateCats([]);
      }
      this.fromAudio = false;
      this.headerComponent.clickAudio();
      this.footerComponent.fetchingDone();
      this.fetching = false;
    } else if (this.fromHomeFooter) {
      if (this.searchKey == '' && this.displaySearchKey == '') {
        this.headerService.setBoth('');
        this.noSearchKey = true;
        this.updatePosts([]);
        this.updateCats([]);
      } else {
        this.getPosts(true);
      }
    } else if (this.categoryId && this.taxonomy && this.postType) {
      if (this.categoriesService.getPosts().length < 1 || this.fromCategory) {
        this.getPosts();
      }
    }
  }

  ngAfterViewInit() {
    this.footerComponent.makeDefault(this.taxonomy); // Taxonomy na siya
  }

  ngOnInit() {
  }
  
  ionViewWillEnter() {
    this.headerService.currHeader = 'search-res';
  }
  
  ionViewWillLeave() {
    this.toastService.closeAny();
  }

  updatePosts(data = {}) {
    // App logic to determine if all data is loaded for loadmore
    // and disable the infinite scroll
    if (this.belowScrollEvent) {
      this.belowScrollEvent.target.complete();
      if (this.currentPage == this.maxPage) {
        this.belowScrollEvent.target.disabled = true;
      }
    }
    
    this.fetching = false;
    this.loadingMore = false;
    this.closeRefresher();
    this.footerComponent.fetchingDone();
    this.geolocation.getCurrentPosition().then((resp) => {
      data = this.categoriesService.insertMiles(data, resp.coords.latitude, resp.coords.longitude);
    });
    this.categoriesService.setPost(data, this.currentPage, this.sortingKey);
  }

  updateCats(data = {}, page = 1) {
    this.categoriesService.setInnerCategories(data, this.currentPage);
  }

  reSort() {
    if (this.sortingKey == 'distance') {
      this.geolocation.getCurrentPosition().then((resp) => {
        this.categoriesService.sortData(this.sortingKey, resp.coords.latitude, resp.coords.longitude);
        this.fetching = false;
      });
    } else {
      this.categoriesService.sortData(this.sortingKey);
      this.fetching = false;
    }
  }

  loadMore(event) {
    setTimeout(() => {
      this.belowScrollEvent = event;
      this.loadingMore = true;
      this.getPosts((this.categoryId && this.taxonomy ? false : true), this.currentPage + 1);
    }, 500);
  }

  getPosts(goForSearch = false, page = 1) {
    if (navigator.onLine) {
      if (page == 1) {
        this.fetching = true;
      }

      if (goForSearch) {
        this.postService.apiPosts(this.searchKey, this.postType, page, this.taxonomy)
          .then((data) => {
            let response = data[0].data;
            let posts = response.posts;
            let categories = response.categories;

            this.catCurrentPage = parseInt(categories.current_page);
            this.catMaxPage = parseInt(categories.max_num_pages);
            if (this.catMaxPage != this.catCurrentPage) {
              this.catTotalItems = parseInt(categories.total);
            }

            this.currentPage = parseInt(posts.current_page);
            this.maxPage = parseInt(posts.max_num_pages);
            this.totalItems = parseInt(posts.total);

            this.updateCats(categories ? this.convertObjectToArr(categories.categories) : [], this.catCurrentPage);
            this.updatePosts(posts ? this.convertObjectToArr(posts.posts) : []);
          })
          .catch((error) => {
            this.updatePosts([]);
            console.log(error);
            // this.errorToast = this.toastService.presentToast(error);
          });
      } else if (this.categoryId && this.taxonomy && this.postType) {
        this.categoriesService.setInnerCategories([]);
        this.categoriesService.apiCategoryPost(this.categoryId, this.taxonomy, this.postType, page)
          .then((data) => {
            let response = data[0];
            this.currentPage = parseInt(response.current_page);
            this.maxPage = parseInt(response.max_num_pages);
            this.totalItems = parseInt(response.total);
            this.updatePosts(response ? this.convertObjectToArr(response.data) : []);
          })
          .catch((error) => {
            this.updatePosts();
            console.log(error);
            // this.errorToast = this.toastService.presentToast('Something went wrong, please restart the app and try again.');
          });
      }
    } else {
      this.errorToast = this.toastService.presentToast('You are offline, please connect to internet to continue.');
      this.fetching = false;
      this.closeRefresher();
      this.categoriesService.setPost([]);
      this.categoriesService.setInnerCategories([]);
      setTimeout(() => {
        this.footerComponent.fetchingDone();
      }, 7000);
    }
  }

  // Called when 3 buttons in the footer was clicked
  goFilter(value) {
    if (this.searchKey == undefined || this.searchKey == '') {
      this.footerComponent.makeDefault(this.taxonomy, true); // taxonomy cat na siya
      this.footerComponent.fetchingDone();
      return this.notEmptySearchAlert();
    } else {
      this.taxonomy = value.tax;
      this.postType = value.posttype;
      this.resetCategories();
      this.getPosts(true);
    }
  }

  // To reset category params, further searching within the page
  resetCategories() {
    this.categoryId = null;
    //this.taxonomy = null;
    this.categoriesService.setPost([]);
    this.categoriesService.setInnerCategories([]);
  }
  
  // Called when the search button in the header was clicked
  goSearch(value) {
    if (value == '') return;
    this.displaySearchKey = this.headerService.searchKey; // .replace('-', ' ')
    this.searchKey = this.headerService.sluggedSearchkey;
    this.headerService.setBoth(value);
    this.noSearchKey = false;
    this.resetCategories();
    this.getPosts(true);
  }

  // Called when blur the search button in the header
  outSearchKey(value) {
    this.displaySearchKey = this.headerService.searchKey; //  != '' ? value.replace('-', ' ') : value
    this.searchKey = this.headerService.sluggedSearchkey;
    if(this.searchKey != '') {
      this.noSearchKey = false;
    }
  }

  goDetails(id, postType) {
    this.fromCategory = false;
    this.router.navigate(['single', {entryId: id, postType: postType}]);
  }

  goCategoryDetails(id, name, slug, taxonomy, post_type) {
    this.categoryId = id;
    this.categoryName = name;
    this.categorySlug = slug;
    this.taxonomy = taxonomy;
    this.postType = post_type;

    this.searchKey = this.headerService.setSluggedSearchKey(this.categoryName);
    this.displaySearchKey = this.headerService.setSearchKey(this.categoryName);

    this.getPosts();
  }

  doRefresh(event) {
    this.refreshEvent = event;
    if (navigator.onLine) {
      if (this.categoryId && this.taxonomy) {
        this.getPosts();
      } else {
        this.getPosts(true);
      }
    } else {
      this.errorToast = this.toastService.presentToast('You are offline, please connect to internet to continue.');
      this.categoriesService.setPost([]);
      this.categoriesService.setInnerCategories([]);
      this.closeRefresher();
    }
  }

  closeRefresher() {
    if (this.refreshEvent) {
      this.refreshEvent.target.complete();
    }
  }

  async notEmptySearchAlert() {
    this.notEmptyAlert = await this.alertCtrl.create({
      header: 'Alert',
      subHeader: '',
      message: 'Search keyword cannot be empty, input your keywords to continue.',
      buttons: ['OK']
    });

    await this.notEmptyAlert.present();
  }

  async showSortingAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Sort Results',
      inputs: [
        {
          name: 'relevance',
          type: 'radio',
          label: 'Relevance',
          value: 'relevance',
          checked : this.sortingKey == 'relevance' ? true : false
        },
        {
          name: 'distance',
          type: 'radio',
          label: 'Distance',
          value: 'distance',
          checked : this.sortingKey == 'distance' ? true : false
        },
        {
          name: 'alphabetical',
          type: 'radio',
          label: 'Alphabetical',
          value: 'alphabetical',
          checked : this.sortingKey == 'alphabetical' ? true : false
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Sort',
          handler: (data) => {
            this.sortingKey = data;
            this.fetching = true;
            this.reSort();
          }
        }
      ]
    });

    await alert.present();
  }
}
