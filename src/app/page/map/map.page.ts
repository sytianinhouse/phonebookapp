import { Component, OnInit, ViewChild, AfterContentInit, Input } from '@angular/core';
import { AbstractComponent } from '../../abstract.component';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ActivatedRoute } from '@angular/router';
import { ToastService } from '../../services/toast.service';
import { ModalController, NavParams } from '@ionic/angular';

declare var google;

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage extends AbstractComponent implements OnInit {

  public map;

  public currlat: number;
  public currlng: number;

  public function: string;
  public postTitle: string;
  public desaddress: string;
  public deslat;
  public deslng;

  @ViewChild('mapElement', { static: false }) mapElement;
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;

  private errorToast: any;
  public fetching: boolean = true;

  constructor(
    public geolocation: Geolocation,
    public route: ActivatedRoute,
    public toastService: ToastService,
    public modalCtrl: ModalController,
    public navParams: NavParams
  ) { 
    super();
    this.function = this.navParams.get('function');
    this.deslat = parseFloat(this.navParams.get('deslat'));
    this.deslng = parseFloat(this.navParams.get('deslng'));
    this.desaddress = this.navParams.get('desaddress');
    this.postTitle = this.navParams.get('postTitle');
    console.log(this.function, this.deslat, this.deslng, this.desaddress, this.postTitle);
  }

  ngOnInit() {
  }

  ionViewWillLeave() {
    this.toastService.closeAny();
  }

  ionViewDidEnter() {
    if (this.function == 'view') {
      return this.viewMarkerMap();
    } else {
      return this.directionMap();
    }
  }

  viewMarkerMap() {
    this.fetching = false;
    this.map = new google.maps.Map(
      this.mapElement.nativeElement,
      {
        center: {lat: this.deslat, lng: this.deslng},
        zoom: 12
      });

    const pos = {
      lat: parseFloat(this.deslat),
      lng: parseFloat(this.deslng)
    };
    console.log(pos);

    this.map.setCenter(pos);

    const marker = new google.maps.Marker({
      position: pos,
      map: this.map,
      title: 'Details',
    });

    const contentString = '<div id="content">' +
      '<div id="siteNotice">' +
      '</div>' +
      '<h3 id="firstHeading" class="firstHeading">'+this.postTitle+'</h3>' +
      '<div id="bodyContent">' +
      '<p>'+this.desaddress+'</p>' +
      '</div>' +
      '</div>';

    const infowindow = new google.maps.InfoWindow({
      content: contentString,
      maxWidth: 400
    });

    marker.addListener('click', function() {
      infowindow.open(this.map, marker);
    });
  }

  directionMap() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.fetching = false;
      this.currlat = resp.coords.latitude;
      this.currlng = resp.coords.longitude;
      console.log(this.currlat, this.currlng);
      const map = new google.maps.Map(
        this.mapElement.nativeElement,
        {
          center: {lat: this.currlat, lng: this.currlng},
          zoom: 8
        }
      );
      this.directionsDisplay.setMap(map);
      
      let desLatLng = new google.maps.LatLng({lat: this.deslat, lng: this.deslng, noWrap: true}); 
      let _self = this;
      this.directionsService.route({
        origin: { lat: _self.currlat, lng: _self.currlng },
        destination: desLatLng,
        travelMode: 'DRIVING'
      }, (response, status) => {
        if (status === 'OK') {
          _self.directionsDisplay.setDirections(response);
        } else {
          console.log(status);
          _self.errorToast = _self.toastService.presentToast('There was an error retrieving your location.');
        }
      });

     }).catch((error) => {
      console.log(error);
      this.errorToast = this.toastService.presentToast('There was an error retrieving your location.');
     });
  }

  closeModal() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
}
