import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AbstractComponent } from '../../abstract.component';
import { PostService } from '../../services/post.service';
import { ToastController, Platform, ModalController, AlertController } from '@ionic/angular';
import { ToastService } from '../../services/toast.service';

import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { HeaderService } from '../../services/header.service';
import { Location } from '@angular/common';
import { MapPage } from '../map/map.page';
import { DomSanitizer } from '@angular/platform-browser';
import { WebView } from '@ionic-native/ionic-webview/ngx';

import * as _ from 'lodash';
import * as moment from 'moment';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-single',
  templateUrl: './single.page.html',
  styleUrls: ['./single.page.scss'],
})
export class SinglePage extends AbstractComponent implements OnInit {

  public post: any;
  public entryId: any;
  public postType: any;
  private errorToast: any;
  private successToast: any;
  public fetching: boolean = true;

  public mapModal: any;

  public days = {
    1 : 'Monday',
    2 : 'Tuesday',
    3 : 'Wednesday',
    4 : 'Thursday',
    5 : 'Friday',
    6 : 'Saturday',
    7 : 'Sunday'
  };

  constructor(
    public share: SocialSharing,
    public router: Router,
    private contacts: Contacts,
    private callNumber: CallNumber,
    private inAppBrowser: InAppBrowser,
    public platform: Platform,
    public route: ActivatedRoute,
    private location: Location,
    public alertCtrl: AlertController,
    public photoViewer: PhotoViewer,
    public sanitizer: DomSanitizer,
    public webview: WebView,
    public modalCtrl: ModalController,
    public postService: PostService,
    private toastCtrl: ToastController,
    public toastService: ToastService,
    public headerService: HeaderService
  ) {
    super();
    this.entryId = this.route.snapshot.params.entryId;
    this.postType = this.route.snapshot.params.postType;

    if (navigator.onLine) {
      this.postService.apiPost(this.entryId, this.postType)
      
        .then((data) => {
          let response = data.data;
          console.log(response);
          this.setPost(response[0]);
        })
        .catch((error) => {
          console.log(error);
          this.errorToast = this.toastService.presentToast('Something went wrong, please restart the app and try again.');
        })
    } else {
      this.errorToast = this.toastService.presentToast('Something went wrong, please restart the app and try again.');
    }
   }

  ngOnInit() {
  }

  ionViewDidEnter() {
  }

  ionViewWillEnter() {
    this.headerService.currHeader = 'single';
  }

  ionViewWillLeave() {
    this.toastService.closeAny();
  }

  setPost(data) {
    this.fetching = false;
    this.post = data;
  }

  async presentToast(message) {
    if (this.errorToast) {
      this.errorToast.dismiss();
    }

    this.errorToast = await this.toastCtrl.create({
      message: message,
      duration: 5000,
      position: 'top',
      color: 'danger'
    });
    this.errorToast.present();
  }

  async goMap(value) {
    this.mapModal = await this.modalCtrl.create({
      component: MapPage,
      componentProps: {
        'function': value,
        'deslat': this.post.lat,
        'deslng': this.post.lng,
        'desaddress': this.post.display_address,
        'postTitle': _.unescape(this.post.post_title)
      }
    });
    return await this.mapModal.present();
  }

  goShare(value) {
    this.share.share(this.post.post_title + ' - ' + value, null, null, null);
  }

  async goAddContact(value, key) {
    const confirm = await this.alertCtrl.create({
      header: 'Are you sure you want to add this in your contact list?',
      message: '',
      buttons: [
        {
          text: 'No',
          handler: () => {
          }
        },
        {
          text: 'Yes',
          handler: () => {
            let contact: Contact = this.contacts.create();

            contact.name = new ContactName(null, ' - ' + key, this.post.post_title);
            contact.phoneNumbers = [new ContactField('mobile', value)];
            return contact.save()
            .then(
              () => {
                this.successToast = this.toastService.successPresentToast('Contact added to your phonebook.');
              },
              (error: any) => {
                this.errorToast = this.toastService.presentToast('Contact not saved.');
              }
            );
          }
        }
      ],
      animated: true
    });
    return await confirm.present();
  }

  goCallNumber(number) {
    this.callNumber.callNumber(number, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  goShareEmail(email) {
    this.share.shareViaEmail('Hi ' + this.post.post_title + ',', 'Phonebook App - Inquiry', email)
      .then(res => console.log('Launched email share!', res))
      .catch(err => console.log('Error launching email share', err));
  }

  goOpenBrowser(url) {
    window.open(url, '_system', 'location=yes');
  }

  goSearch(value: any) {
    this.headerService.setSearch(true);
    this.location.back();
  }

  audioClicked(value: any) {
    this.headerService.setAudio(true);
    this.location.back();
  }

  trustUrl(url) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  goSocialmedia(url) {
    window.open(url, '_system', 'location=yes');
  }

  formatTime(secs) {
    const formatted = moment.utc(secs * 1000).format('hh:mm a');

    return formatted;
  }

  viewImage(url) {
    this.photoViewer.show(url);
  }
}
