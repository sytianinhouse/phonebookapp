import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SinglePageRoutingModule } from './single-routing.module';

import { SinglePage } from './single.page';
import { SharedModule } from '../../partials/shared/shared.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MapPage } from '../map/map.page';
import { MapPageModule } from '../map/map.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    FontAwesomeModule,
    SinglePageRoutingModule,
    MapPageModule
  ],
  declarations: [
    SinglePage
  ]
})
export class SinglePageModule {}
