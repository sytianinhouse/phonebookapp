import { Injectable } from '@angular/core';
import { AppService } from './app.service';
import { HTTP } from '@ionic-native/http/ngx';
import { ToastService } from './toast.service';

@Injectable({
  providedIn: 'root'
})
export class PostService extends AppService {

  private post: any;
  
  constructor(
    private HTTP: HTTP,
    public toastService: ToastService
  ) {
    super();
  }

  apiPost(id, postType) {
    if (navigator.onLine) {
      let URL = this.APP_URL + '/'+ postType +'/' + id;

      return this.HTTP.get(URL, {}, {})
        .then(this.extractData)
        .catch(this.handleError)
    } else {
      this.toastService.presentToast('You are offline, please connect to internet to continue.');
    }
    
  }

  apiPosts(value, postType, page = 1, taxonomy = null) {
    if (navigator.onLine) {
      // let URL = this.APP_URL + '/app-search/'+ postType +'/' + value + '/posts-per-page/30/page/' + page;
      let URL = this.APP_URL + '/app-search-v2/'+ taxonomy +'/'+ postType +'/'+ value +'/posts-per-page/50/page/' + page;

      return this.HTTP.get(URL, {}, {})
        .then(this.extractData)
        .catch(this.handleError)
    } else {
      this.toastService.presentToast('You are offline, please connect to internet to continue.');
    }
  }
}
