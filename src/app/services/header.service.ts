import { Injectable } from '@angular/core';
import { AppService } from './app.service';

import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class HeaderService extends AppService {

  public searchKey: any;
  public sluggedSearchkey: any;
  public currHeader: any;

  public navigatedAudio: boolean = false;
  public navigatedSearch: boolean = false;

  constructor() {
    super();
  }

  setSearchKey(value) {
    this.searchKey = _.unescape(value);

    return this.searchKey;
  }

  setSluggedSearchKey(value) {
    value = this.htmlDecode(value);
    this.sluggedSearchkey = value.replace(/\s+/g, '%20');

    return this.sluggedSearchkey;
  }

  setBoth(value) {
    this.setSearchKey(value);
    this.setSluggedSearchKey(value);
  }

  setAudio(value) {
    this.navigatedAudio = value;
  }

  setSearch(value) {
    this.navigatedSearch = value;
  }
}
