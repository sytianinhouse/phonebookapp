import { Injectable } from '@angular/core';

import { AppService } from './app.service';
import { HTTP } from '@ionic-native/http/ngx';
import { ToastService } from './toast.service';

import * as _ from 'lodash';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService extends AppService {

  private categories = [];
  private innerCategories = [];
  private posts = [];

  constructor(
    public HTTP: HTTP,
    private toastService: ToastService,
    public geolocation: Geolocation
  ) {
    super();
  }
  
  apiCategories(withType = null, page = 1) {

    let URL = '';
    switch(withType) { 
      case 'listings_dir_cat': { 
        // URL = this.APP_URL + '/taxonomy/listings_dir_cat/false';
        URL = this.APP_URL + '/taxonomy-v2/listings_dir_cat/posts-per-page/24/page/' + page + '/';
        break; 
      } 
      case 'white_lists_dir_cat': { 
        // URL = this.APP_URL + '/taxonomy/white_lists_dir_cat/false';
        URL = this.APP_URL + '/taxonomy-v2/white_lists_dir_cat/posts-per-page/24/page/' + page + '/';
        break; 
      } 
      case 'gov_lists_dir_cat': { 
        // URL = this.APP_URL + '/taxonomy/gov_lists_dir_cat/false';
        URL = this.APP_URL + '/taxonomy-v2/gov_lists_dir_cat/posts-per-page/24/page/' + page + '/';
        break; 
      } 
      default: {
        URL = this.APP_URL + '/all-terms';
        break;
      }
    } 

    if (navigator.onLine) {
      return this.HTTP.get(URL, {}, {})
        .then(this.extractData)
        .catch(this.handleError)
    } else {
      this.toastService.presentToast('You are offline, please connect to internet to continue.');
    }
  }

  apiAds() {
    let URL = this.APP_URL + '/get-all-posts/ypi_ads/';

    if (navigator.onLine) {
      return this.HTTP.get(URL, {}, {})
        .then(this.extractData)
        .catch(this.handleError)
    } else {
      this.toastService.presentToast('You are offline, please connect to internet to continue.');
    }
  }

  getCategories() {
    return this.categories;
  }

  setCategories(data, page = 1) {
    let addon = data;
    if (data && data.length > 0) {
      addon = this.deepArraySort(data, 'name');
    }

    if (page == 1) {
      this.categories = addon;
    } else {
      this.categories = _.concat(this.categories, addon);
    }
  }

  getInnerCategories() {
    return this.innerCategories;
  }

  setInnerCategories(data, page = 1) {
    let addon = data;
    if (data && data.length > 0) {
      addon = this.deepArraySort(data, 'name');
    }

    if (page == 1) {
      this.innerCategories = addon;
    } else {
      this.innerCategories = _.concat(this.innerCategories, addon);
    }
  }

  apiCategoryPost(termId, taxonomy, postType, page = 1) {
    let URL = this.APP_URL +'/'+ postType +'/'+ taxonomy +'/'+ termId + '/posts-per-page/30/page/' + page + '/';

    console.log(URL);
    if (navigator.onLine) {
      return this.HTTP.get(URL, {}, {})
        .then(this.extractData)
        .catch(this.handleError)
    } else {
      this.toastService.presentToast('You are offline, please connect to internet to continue.');
    }
  }

  getPosts() {
    return this.posts;
  }

  setPost(data, page = 1, sortKey = 'post_title') {
    let addon = data;

    if (data && data.length > 0) {
      addon = data;
    }  

    if (page == 1) {
      this.posts = addon;
    } else {
      this.posts = _.concat(this.posts, addon);
    }

    if (sortKey == 'distance') {
      this.geolocation.getCurrentPosition().then((resp) => {
        this.sortData(sortKey, resp.coords.latitude, resp.coords.longitude);
      });
    } else {
      this.sortData(sortKey);
    }
  }

  sortData(sortKey, lat = null, lng = null) {
    this.posts = this.deepArraySort(this.posts, sortKey, lat, lng);
  }

  insertMiles(data, lat, lng) {
    data = _.map(data, ((a) => {

      let LatLng = true;
      let within: any;
      if ((!a.lat && !a.lng) || (a.lat == 0 && a.lng == 0)) {
        within = 999999999;
        LatLng = false;
      } else {
        within = this.getDistanceSort(
          parseFloat(lat), 
          parseFloat(lng), 
          parseFloat(a.lat), 
          parseFloat(a.lng),
          true
        );
      }

      a['has_distance'] = LatLng;
      a['calculated_distance'] = within;
      a['calculated_miles'] = (within * 0.621371).toFixed(0);
      return a;
    }));

    return data;
  }
}
