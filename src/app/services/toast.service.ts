import { Injectable } from '@angular/core';
import { AppService } from './app.service';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService extends AppService {

  public errorToast: any;
  public successToast: any;

  constructor(
    private toastCtrl: ToastController
  ) { 
    super();
  }

  async presentToast(message) {

    if (this.errorToast) {
      this.errorToast.dismiss();
    }

    this.errorToast = await this.toastCtrl.create({
      message: message,
      duration: 5000,
      position: 'top',
      color: 'danger'
    });
    return this.errorToast.present();
  }

  async successPresentToast(message) {

    if (this.successToast) {
      this.successToast.dismiss();
    }

    this.successToast = await this.toastCtrl.create({
      message: message,
      duration: 5000,
      position: 'top',
      color: 'success'
    });
    return this.successToast.present();
  }

  closeAny() {
    if (this.successToast) {
      this.successToast.dismiss();
    }

    if (this.errorToast) {
      this.errorToast.dismiss();
    }
  }
}
