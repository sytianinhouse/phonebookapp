import { Injectable, OnInit } from '@angular/core';

import { CONFIG } from '../../config';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  public currScreenOrientation: any;

  public APP_VERSION = CONFIG.appVersion;
  public APP_URL = CONFIG.apiUrl;

  constructor() { 
    
  }

  // Extraction of data from JSON
  public extractData(res: any) {

    let status = res.status;
    let data = JSON.parse(res.data); // error message as string
    let headers = res.headers;
    
    return data || {};
  }

  // Extraction of data from JSON and handle error response
  public handleError(error: any) {

    let status = error.status;
    let data = JSON.parse(error.error); // error message as string
    let headers = error.headers;

    const errMsg = (data.message) ? data.message :
    status ? `Error - ${status}` : 'Server error';

    throw new Error(errMsg);
  }

  /** Helper Methods */
  deepArraySort(array, key, lat = null, lng = null) {
    
    if (key == 'relevance') {
      array.sort( (a, b) => {
        if(a['featured_item'].length < b['featured_item'].length) { return 1; }
        if(a['featured_item'].length > b['featured_item'].length) { return -1; }
        return 0;
      });
    } else if(key == 'distance') { 
      // array.sort( (a) => {
      //   if ((!a.lat && !a.lng) || (a.lat == 0 && a.lng == 0)) {
      //     return 1;
      //   }

      //   let within = this.getDistanceSort(
      //     parseFloat(lat), 
      //     parseFloat(lng), 
      //     parseFloat(a.lat), 
      //     parseFloat(a.lng)
      //   );
      //   console.log(a.post_title, a.lat, a.lng);
      //   console.log(within);

      //   return within ? -1 : 1;
      // });
      array.sort( (a, b) => {
        if(a['calculated_distance'] < b['calculated_distance']) { return -1; }
        if(a['calculated_distance'] > b['calculated_distance']) { return 1; }
        return 0;
      });
    } else {
      key = 'post_title';
      array.sort( (a, b) => {
        if(a[key] < b[key]) { return -1; }
        if(a[key] > b[key]) { return 1; }
        return 0;
      });
    }

    return array;
  }

  getDistanceSort(lat1, lng1, lat2, lng2, number = false) {
    // let R = 6371; // Earth's radius in Km
    // let distance = Math.acos(Math.sin(lat1)*Math.sin(lat2) + 
    //                 Math.cos(lat1)*Math.cos(lat2) *
    //                 Math.cos(lng2-lng1)) * R;

    var R = 6371; // km
    // var dLat = this.toRad(lat2-lat1);
    // var dLon = this.toRad(lng2-lng1);
    // var dlat1 = this.toRad(lat1);
    // var dlat2 = this.toRad(lat2);

    lat1 = this.toRad(lat1);
    lng1 = this.toRad(lng1);
    lat2 = this.toRad(lat2);
    lng2 = this.toRad(lng2);

    // console.log(dLat, dLon, dlat1, dlat2)
    // console.log(lat1, lng1, lat2, lng2)

    // var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
    //         Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
    // var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    // var d = R * c;

    // let distance = Math.acos(Math.sin(lat1)*Math.sin(lat2) + 
    //                 Math.cos(lat1)*Math.cos(lat2) *
    //                 Math.cos(lng2-lng1)) * R;

    let dlon = lng2 - lng1;  
    let dlat = lat2 - lat1; 
    let a = Math.pow(Math.sin(dlat / 2), 2) 
              + Math.cos(lat1) * Math.cos(lat2) 
              * Math.pow(Math.sin(dlon / 2),2); 
    let c = 2 * Math.asin(Math.sqrt(a));
    let distance = c * R;

    if (number) {
      return distance;
    }
    
    if (distance <= 100){
      return true;
    } else {
      return false;
    }
  }

  toRad(Value) 
  {
      return Value * (Math.PI / 180);
  }

  htmlEncode(string) {
    var buf = [];
			
			for (var i=string.length-1;i>=0;i--) {
				buf.unshift(['&#', string[i].charCodeAt(), ';'].join(''));
			}
			
			return buf.join('');
  }

  htmlDecode(string) {

    var entityMap = {
      "&amp;" : "&",
      "&lt;" : "<",
      "&gt;" : ">",
      '&quot;' : '"',
      '&#39;' : "'",
      '&#x2F;' : "/"
    };

    return String(string).replace(/[&<>"'\/]/g, function (s) {
      return entityMap[s];
    });
    
    // return string.replace(/&#(\d+);/g, function(match, dec) {
    //   return String.fromCharCode(dec);
    // });
  }
}
