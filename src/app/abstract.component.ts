import { Platform } from '@ionic/angular';

import * as moment from 'moment';
import * as _ from 'lodash';

export abstract class AbstractComponent {

  public disableBehaviorClass = 'disable-touch-behavior';
  
  public countObject(collection) {
    return _.size(collection);
  }

  public limitString(str, maxLength = 20) {
    return (str.length > maxLength)? str.substring(0,maxLength) +'..' : (str)
  }

  public createRange(number){
    var items: number[] = [];
    for(var i = 1; i <= number; i++){
       items.push(i);
    }
    return items;
  }

  public unescapeString(string) {
    return _.unescape(string);
  }

  public className() { return this.constructor.name.toLowerCase().replace('component', ''); }

  public doComponentCheck() {
    return this.className();
  }

  public addCommas(nStr){
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
     x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
   }

  convertObjectToArr(object) {
    return _.values(object);
  }

  arrShuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
  
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
  
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
  
    return array;
  }
}
