export let CONFIG = {

    /**
     * All changes are considered increase of 1 in version number
     */
    appVersion: '5.2',
  
    // For production & demo
    apiUrl: 'https://guamphonebook.com/wp-json/wp/v2',
  
    // Last update date
    lastUpdate: '2020-01-24',
  
  };
  